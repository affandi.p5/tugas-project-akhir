{
            "@context": "https://schema.org",
            "@graph": [{
                "@type": ["Person", "Organization"],
                "@id": "http://www.iintrans.com/#/schema/person/13c9d5b8db51f7c5405e085efb13f761",
                "name": "pemilik",
                "image": {
                    "@type": "ImageObject",
                    "@id": "http://www.iintrans.com/#personlogo",
                    "inLanguage": "id-ID",
                    "url": "http://0.gravatar.com/avatar/ff3f7c523f33b1b9b00b77a6d56f9f28?s=96&d=mm&r=g",
                    "contentUrl": "http://0.gravatar.com/avatar/ff3f7c523f33b1b9b00b77a6d56f9f28?s=96&d=mm&r=g",
                    "caption": "pemilik"
                },
                "logo": {
                    "@id": "http://www.iintrans.com/#personlogo"
                },
                "description": "Iin Trans adalah Perusahaan Rental mobil yang berlokasi di Jakarta dengan layanan 24 jam, harga murah, profesional dan terpercaya. Kami melayani sewa mobil dan rental mobil untuk berbagai keperluan diantaranya: keperluam bisnis, keperluan wisata dan keperluan pribadi. Rental mobil Iin Trans juga melayani rental mobil ke luar kota seperti ke Yogyakarta, Solo, Semarang, Surabaya, Madura dan Bali dengan armada mobil bersih terawat. Armada mobil terlengkap di dukung driver yang memiliki wawasan luas, ramah dan profesional menjadikan Anda aman dan nyaman selama di perjalanan.",
                "sameAs": ["http://www.iintrans.com", "Berkah Rental"]
            }, {
                "@type": "WebSite",
                "@id": "http://www.iintrans.com/#website",
                "url": "http://www.iintrans.com/",
                "name": "Iin Trans 0813-8761-8977, Rental Mobil Murah Jakarta",
                "description": "Melayani Rental Mobil Murah Tangerang, Rental Mobil Murah Jakarta Barat, Rental Mobil Murah Jakarta Timur, Rental Mobil Jakarta Pusat, Rental Mobil Jakarta Selatan, Rental Mobil Murah Tangerang, Rental Mobil Murah Jabodetabek, Rental Mobil Terdekat, Sewa Mobil Mewah, Sewa Mobil Wisata Jakarta, Sewa Mobil Jakarta, Sewa Mobil Murah 24 jam Jakarta",
                "publisher": {
                    "@id": "http://www.iintrans.com/#/schema/person/13c9d5b8db51f7c5405e085efb13f761"
                },
                "potentialAction": [{
                    "@type": "SearchAction",
                    "target": {
                        "@type": "EntryPoint",
                        "urlTemplate": "http://www.iintrans.com/?s={search_term_string}"
                    },
                    "query-input": "required name=search_term_string"
                }],
                "inLanguage": "id-ID"
            }, {
                "@type": "CollectionPage",
                "@id": "http://www.iintrans.com/rental/#webpage",
                "url": "http://www.iintrans.com/rental/",
                "name": "Rental Archive - Iin Trans 0813-8761-8977, Rental Mobil Murah Jakarta",
                "isPartOf": {
                    "@id": "http://www.iintrans.com/#website"
                },
                "breadcrumb": {
                    "@id": "http://www.iintrans.com/rental/#breadcrumb"
                },
                "inLanguage": "id-ID",
                "potentialAction": [{
                    "@type": "ReadAction",
                    "target": ["http://www.iintrans.com/rental/"]
                }]
            }, {
                "@type": "BreadcrumbList",
                "@id": "http://www.iintrans.com/rental/#breadcrumb",
                "itemListElement": [{
                    "@type": "ListItem",
                    "position": 1,
                    "name": "Home",
                    "item": "http://www.iintrans.com/"
                }, {
                    "@type": "ListItem",
                    "position": 2,
                    "name": "Rental"
                }]
            }]
        }
