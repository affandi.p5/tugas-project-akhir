<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelOrder extends Model
{
    use HasFactory;
    protected $table = "transaksi";
    protected $fillable = ["id","nama","total","nohp","tanggal","user_id","mobil_id"];
}
