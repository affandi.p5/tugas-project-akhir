<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelMerk extends Model
{
    use HasFactory;
    protected $table = "merk";
    protected $fillable = ["id","nama"];
}
