<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelMobil extends Model
{
    use HasFactory;
    protected $table = "mobil";
    protected $fillable = ["nama", "merk_id", "harga", "tahun", "deskripsi", "gambar"];
}
