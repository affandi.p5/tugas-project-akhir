<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function homepage(){
        return redirect('/home');
    }

    public function admin(){
        return view('admin.master');
    }
}
