<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ModelMobil;
use App\Models\ModelMerk;
use File;

class MobilCRUDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mobil = ModelMobil::all();
        return view("admin.mobil", ['mobil' => $mobil]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $merk = ModelMerk::all();
        return view("admin.tambah", ['merk' => $merk]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'gambar' => 'required|mimes:jpg,bmp,png|max:3048',
            'merk' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
        ]);
        
        $file= $request->file('gambar');
        $filename= date('YmdHi').".".$file->extension();
        $file-> move(public_path('image'), $filename);

        $mobil = new ModelMobil;

        $mobil->nama = $request->nama;
        $mobil->harga = $request->harga;
        $mobil->deskripsi = $request->deskripsi;
        $mobil->gambar = $filename;
        $mobil->tahun = $request->tahun;
        $mobil->merk_id = $request->merk;

        $mobil->save();

        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mobil = ModelMobil::find($id);
        return view('admin.show', ['mobil' => $mobil]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobil = ModelMobil::find($id);
        $merknya = $mobil->merk_id;
        $merk = ModelMerk::find($merknya);
        $merk2 = ModelMerk::all();
        return view('admin.edit', ['mobil' => $mobil, 'merk'=>$merk, 'merk2'=>$merk2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
             'gambar' => 'mimes:jpg,bmp,png|max:3048',
            'merk' => 'required',
            'deskripsi' => 'required',
            'tahun' => 'required',
        ]);
    
        $mobil = ModelMobil::find($id);
        $merk = ModelMerk::find($id);
        $mobil->nama = $request->nama;
        $mobil->deskripsi = $request->deskripsi;
        $mobil->harga = $request->harga;
        // $mobil->poster = $request->poster;
        if($request->has('gambar')){
            $path = 'image/';

            File::delete($path. $mobil->gambar);

            $file2 = $request->file('gambar');
            $filename2 = date('YmdHi').".".$file2->extension();
            $file2-> move(public_path('image'), $filename2);

            $mobil->gambar = $filename2;

            $mobil->save();

        }
        $mobil->merk_id = $request->merk;
        $mobil->tahun = $request->tahun;
        $mobil->save();
        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mobil = ModelMobil::find($id);
        $path = 'image/';
        File::delete($path. $mobil->gambar);
        $mobil->delete();
        return redirect('/admin');
        // done
    }
}
