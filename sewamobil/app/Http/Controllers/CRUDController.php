<?php

namespace App\Http\Controllers;
use App\Models\ModelOrder;
use App\Models\ModelMobil;

use Illuminate\Http\Request;
use DB;

class CRUDController extends Controller
{
    public function create($id){

        $mobil = DB::table('mobil')->find($id);
        $merk = DB::table('merk')->find($mobil->merk_id);
        return view("userview.booking", ['mobil' => $mobil], ['merk'=>$merk]);

    }

    public function store(Request $request){

        $request->validate([
            'nama' => 'required',
            'nohp' => 'required',
            'tanggal' => 'required'
        ]);
        
        DB::table('transaksi')->insert([
            'nama' => $request['nama'],
            'total' => $request['harga'],
            'tanggal' => $request['tanggal'],
            'nohp' => $request['nohp'],
            'user_id' => '1',
            'mobil_id' => $request['mobil_id'],
        ]);

        // // $order = DB::table('transaksi')->get();
        // $order = new ModelOrder; 

        // $order->nama = $request->nama;
        // $order->total = $request->harga;
        // $order->tanggal = $request->tanggal;
        // $order->nohp = $request->nohp;
        // $order->user_id = '1';
        // $order->mobil_id = $request->mobil_id;

        return redirect('/home');

    }
}
