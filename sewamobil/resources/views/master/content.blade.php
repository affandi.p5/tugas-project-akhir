
{{-- //// --}}
@extends('master.master')
@section('content')
<div id="headerwrap">
    <div id="header">
        <div class="headlogo">
            <a href="/"><strong>SANBERCODE</strong></a>
            <p>Profesional Terpercaya Buka 24 jam</p>
        </div>
        <div class="headinfo">
            <strong class="big"><span style="font-size:80%; color: black">Hai, Selamat Datang $USER</span> </strong>
        </div>
        <div style="clear: both"></div>
    </div>
</div>


<!-- Features Start -->
<div class="container">
    <div class="d-flex flex-column text-center mb-5">
        <h1 class="text-secondary mb-3">Tentang Kami</h1>
    </div>
    <div class="row align-items-center">
        <div class="col-lg-7 py-5 py-lg-0 px-3 px-lg-5">
            <h4 class="text-secondary mb-3">Mengapa memilih kami?</h4>
            <p>SANBERCODE - Merupakan perusahaan penyedia jasa transportasi rentalmobil dan sewa murah di jakarta. Kami menyediakan berbagai jenis type dan keluaran terbaru.</p>
            <h5><b>Profesional</b></h5>
            <p class="mb-4">Kami bekerja dengan profesional dan sesuai dengan prosedur untuk mencapai kepuasan pelanggan.</p>
            <h5><b>Banyak Pilihan Mobil</b></h5>
            <p class="mb-4">Kami juga menyediakan mobil seperti Avanza, Xenia, Innova, Luxio, Fortuner, Venturer, Hiace, Alphard, Elf, bahkan Bus.</p>
            <h5><b>Harga Relatif Terjangkau</b></h5>
            <p class="mb-4">Kami memberikan harga yang terjangkau bagi pelanggan tanpa mengurangi kuantitas dan kualitas pelayanan.</p>
        </div>
    </div>
</div>
<!-- Features End -->

<!-- carousel Start -->

<!-- Carousel End -->
@endsection