@extends('admin.master')

@section('title')
Tambah Merk
@endsection

@section('contentadmin')

 <!-- Divider -->
 <hr class="sidebar-divider my-0">

<a href="../admin" class="btn btn-danger my-3">Kembali</a>


<form action="/admin/merk" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input name="nama" type="text" class="form-control">
    </div>

    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>



@endsection
