@extends('admin.master')

@section('title')
Merk
@endsection

@section('contentadmin')


<a href="{{ URL::to('admin/merk/create') }}" class="btn btn-primary my-3">Tambah Merk</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Pemesan</th>
            <th scope="col">Total</th>
            <th scope="col">No Hp</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Mobil</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($transaksi as $key=>$item)
        <tr>
            @php
            //ambil data mobil dari mobil_id
               $a = App\Models\ModelMobil::find($item->mobil_id);
            @endphp
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->total}}</td>
            <td>{{$item->nohp}}</td>
            <td>{{$item->tanggal}}</td>
            <td>{{$a->nama}}</td>
            <td>
                <form action="../../admin/transaksi/{{$item->id}}" method="POST">
                    @csrf @method('delete')

                    <input type="submit" class="btn btn-danger" value="Cancel Order">
                </form>
            </td>
        </tr>

        @empty

        @endforelse

    </tbody>
</table>


@endsection
