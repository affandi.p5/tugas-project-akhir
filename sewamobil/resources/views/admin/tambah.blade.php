@extends('admin.master')

@section('title')
Tambah Mobil
@endsection

@section('contentadmin')

 <!-- Divider -->
 <hr class="sidebar-divider my-0">

<a href="../admin" class="btn btn-danger my-3">Kembali</a>


<form action="/admin" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input name="nama" type="text" class="form-control">
    </div>

    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Harga</label>
        <input name="harga" type="number" class="form-control">
    </div>

    @error('harga')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Deskripsi</label>
        <textarea name="deskripsi" class="form-control" rows="3"></textarea>
    </div>

    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <select name="merk" class="custom-select custom-select-lg mb-3">
        <option value="">Silahkan Pilih Merk</option>
        @forelse ($merk as $item)
        <option value={{$item->id}}>{{$item->nama}}</option>
        
        @empty
            
        @endforelse
       
    </select>

    @error('merk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <br>

    <div class="custom-file">
        <input name="gambar" type="file" class="custom-file-input" id="customFile">
        <label class="custom-file-label" for="inputGroupFile02">Pilih Gambar</label>
    </div>

    @error('gambar')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group" style="margin-top: 10">
        <label>Tahun</label>
        <input name="tahun" type="number" class="form-control">
    </div>

    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>



@endsection

{{-- untuk upload file --}}
@push('uploadfile')
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

</script>

<script>
    $('#inputGroupFile02').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
</script>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

@endpush