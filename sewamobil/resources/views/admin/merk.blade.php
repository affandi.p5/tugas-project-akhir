@extends('admin.master')

@section('title')
Merk
@endsection

@section('contentadmin')


<a href="{{ URL::to('admin/merk/create') }}" class="btn btn-primary my-3">Tambah Merk</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Merk</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($merk as $key=>$item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>
                <form action="merk/{{$item->id}}" method="POST">
                    <a class="btn btn-primary" href="merk/{{$item->id}}/edit">Edit</a>
                    @csrf @method('delete')

                    <input type="submit" class="btn btn-danger" value="Hapus">
                </form>
            </td>
        </tr>

        @empty

        @endforelse

    </tbody>
</table>


@endsection
