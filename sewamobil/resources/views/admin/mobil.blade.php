@extends('admin.master')

@section('title')
Mobil
@endsection

@section('contentadmin')


<a href="admin/create" class="btn btn-primary my-3">Tambah Mobil</a>

<div class="container">
    <div class="row">
        @forelse ($mobil as $item)

        <div class="col-sm">
              
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src={{asset('image/'.$item->gambar)}} alt="Card image cap" height="300">
                <div class="card-body">
                  <h1>{{$item->nama}}</h1>
                  <p class="card-text">{{Str::limit($item->harga , 100, ' ...')}}</p>
                  <p class="card-text">{{Str::limit($item->deskripsi , 100, ' ...')}}</p>
                  <a href="/admin/{{$item->id}}" class="btn btn-primary btn-block btn-sm">Read More</a>
                  <a href="/admin/{{$item->id}}/edit" class="btn btn-warning btn-block btn-sm">Edit</a>
                  <form action="/admin/{{$item->id}}" method="post">
                    @method('delete');
                    @csrf
                    <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                  </form>

                </div>
              </div>
        
        
          </div>
          
        @empty
            
        @endforelse
        
    </div>
  </div>



@endsection