@extends('admin.master')

@section('title')
Detail Mobil {{$mobil->nama}}
@endsection


@section('contentadmin')

<h1>{{$mobil->nama}}</h1>
<img src="{{asset('/image/'.$mobil->gambar)}}" class="tengah" alt="">
<p>{{$mobil->deskripsi}}</p>

<a href="/admin" class="btn btn-danger">Kembali</a>

@endsection