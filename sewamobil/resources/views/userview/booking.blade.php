@extends('master.master')

@section('title')
Tambah Mobil
@endsection

@section('content')

 <!-- Divider -->
 <hr class="sidebar-divider my-0">

<a href="../home" class="btn btn-danger my-3">Kembali</a>


<form action="/order" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input name="1" value="{{$mobil->nama}}" disabled type="text" class="form-control">
    </div>


   
    <div class="form-group">
        <label>Harga</label>
        <input name="3" value="{{$mobil->harga}}" disabled type="number" class="form-control">
    </div>


    <div class="form-group">
        <label for="exampleFormControlTextarea1">Deskripsi</label>
        <textarea name="4" class="form-control" rows="3" disabled>{{$mobil->deskripsi}}</textarea>
    </div>

    {{-- nyimpen variabel dari db : mobil --}}

    <div class="form-group">
        <input name="namamobil" value="{{$mobil->nama}}" type="hidden" class="form-control">
    </div>

    <div class="form-group">
        <input name="mobil_id" value="{{$mobil->id}}" type="hidden" class="form-control">
    </div>
   
    <div class="form-group">
        <input name="harga" value="{{$mobil->harga}}" type="hidden" class="form-control">
    </div>

    {{-- nyimpen variabel dari db : admin --}}

    <br>
    
    <h3>Merk : {{$merk->nama}}</h3>

    <br>


    <div class="form-group" style="margin-top: 10">
        <label>Tahun</label>
        <input name="tahun" disabled value="{{$mobil->tahun}}" type="number" class="form-control">
    </div>

    <br><br>

    <h1>Harap Isi Form Booking</h1>

    <br><br>

    <div class="form-group" style="margin-top: 10">
        <label>Nama Pemesan</label>
        <input name="nama" type="text" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group" style="margin-top: 10">
        <label>Nomor Hp</label>
        <input name="nohp" type="number" class="form-control">
    </div>
    @error('nohp')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group" style="margin-top: 10">
        <label>Tanggal Booking</label>
        <input name="tanggal" type="date" class="form-control">
    </div>
    @error('tanggal')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>



@endsection
