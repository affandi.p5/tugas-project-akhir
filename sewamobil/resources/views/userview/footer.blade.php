<div class="footer">
    <div class="footbarwidget">
        <div class="footbar">
            <h4>Telepon atau Wa</h4>
            <div class="textwidget">
                <p><strong>CS 1: 0813-8761-8977</strong></p>
                <p><strong>CS 2: 0821-2209-8688</strong></p>
                <p><strong>CS 3: 0878-8262-0786</strong></p>
            </div>
        </div>
    </div>
    <div class="footbarwidget">
        <div class="footbar">
            <h4>Pembayaran</h4>
            <div class="textwidget">
                <p><strong>Bank BCA a/n&nbsp; &nbsp;Iin Karsono 3451
                        7482 10</strong></p>
                <p><strong>Bank Niaga a/n Iin Karsono 7611 21899
                        700</strong></p>
            </div>
        </div>
    </div>
    <div class="footbarwidget">
        <div class="footbar">
            <h4>Addres</h4>
            <div class="textwidget">
                <p><strong>Perum Taman Kota Permai 1, Rt 02/06 Blok
                        A2,No 21, Karet, Sepatan Tangerang 15132</strong></p>
            </div>
        </div>
    </div>
    <div class="copyright">
        <a href="#">
            <h5>SANBERCODE RENTAL MOBIL</h5>
        </a> <span class="small">www.sanbercode.com</span>
    </div>

</div>
