<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MobilCRUDController;
use App\Http\Controllers\MobilUserController;
use App\Http\Controllers\MobilOrderController;
use App\Http\Controllers\CRUDController;
use App\Http\Controllers\MerkController;
use App\Http\Controllers\TransaksiController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/admin/merk', MerkController::class);
Route::resource('/admin/transaksi', TransaksiController::class);
Route::resource('admin', MobilCRUDController::class);
Route::resource('/home', MobilUserController::class);

//insert database order
Route::get('/order/create/{mobil}', [CRUDController::class, 'create']);
Route::post('/order', [CRUDController::class, 'store']);


// Route::resource('/order', MobilOrderController::class);


Route::get('/', [HomeController::class,'homepage']);
// Route::get('/admin', [HomeController::class,'admin']);
Route::get('/daftar', [Controller::class,'daftar']);
// Route::get('/armada', [Controller::class,'armada']);
Route::get('/testmaster', function () {
    return view('master.master');

});


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
